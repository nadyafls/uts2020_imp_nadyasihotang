#!/usr/bin/env python
# coding: utf-8

# In[ ]:


Soal 3: (bobot 10)
# Using keys and indexing, grab the 'hello' from the following
dictionaries:
d1 = {'simple_key':'hello'}
d2 = {'k1':{'k2':'hello'}}
d3 = {'k1':[{'nest_key':['this is deep',['annyeong','hello']]}]}


# In[1]:


#nmr3
d1 = {'simple_key':'hello'}
d2 = {'k1':{'k2':'hello'}}
d3 = {'k1':[{'nest_key':['this is deep',['annyeong','hello']]}]}
d11 = list (d1)
print(d1['simple_key'])
print(d2['k1']['k2'])
print(d3['k1'][0]['nest_key'][1][1])


# In[ ]:




