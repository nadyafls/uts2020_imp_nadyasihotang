#!/usr/bin/env python
# coding: utf-8

# In[ ]:


Soal 8: (bobot 10)
# Given two strings, return True if either of the strings appears in the
# beginning of the other string, ignoring upper/lower case differences (in
# other words, the
# computation should not be "case sensitive").
# Note: s.lower() returns the lowercase version of a string.
# Examples:
# begin_other('abcKopi', 'abc') → True
# begin_other('AbC', 'HiaBc') → False
# begin_other('abc', 'abcXabc') → True
# begin_other('abc', 'abXabc') → False
def begin_other(a, b):
 # CODE GOES HERE


# In[1]:


#nmr 8
def begin_other(str1, str2):
  return str1.lower()[len(str2)-1:] == str2.lower()
begin_other('abc', 'abcXabc')


# In[ ]:




